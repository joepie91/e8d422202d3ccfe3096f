/* Follow-up from https://gist.github.com/joepie91/211c8e99fb5a83b76079, which shows promise flattening */
 
Promise.try(function(){
	return outerThingOne();
}).then(function(value){
	if (someCondition === true) {
		return innerThingOne();
	} else {
		return Promise.try(function(){
			return alternativeInnerThingOne();
		}).then(function(subValue){
			return doSomethingWith(subValue);
		})
	}
}).then(function(result){
	return outerThingThree(result);
});